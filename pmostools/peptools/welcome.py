"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This script is used to welcome the user to the system
"""

import os
import tkinter as tk
import ttkbootstrap as ttk
import welconf
import welfunc


# setting up window
pewel = welconf.bbstyle
pewel.resizable(False, False)
WINDOW_HEIGHT = 400
WINDOW_WIDTH = 710


def wbase():
    """Check to see what base is being loaded"""
    if os.path.exists("/etc/devuan_version"):
        pewel.title(welconf.DEVUAN_TITLE)
    elif os.path.exists("/etc/debian_version"):
        pewel.title(welconf.DEBIAN_TITLE)

def center_screen():
    """ gets the coordinates of the center of the screen """
    screen_width = pewel.winfo_screenwidth()
    screen_height = pewel.winfo_screenheight()
    # Coordinates of the upper left corner of the window to make the window
    # appear in the center
    x_cordinate = int((screen_width / 2) - (WINDOW_WIDTH / 2))
    y_cordinate = int((screen_height / 2) - (WINDOW_HEIGHT / 2))
    pewel.geometry("{}x{}+{}+{}".format(WINDOW_WIDTH,
                   WINDOW_HEIGHT, x_cordinate, y_cordinate))


def kon():
    """ This feature will disabled the welcome screen"""
    # This will get the logged in user
    gusr = tk.StringVar()
    gusr.set(os.getlogin())
    # set it in to as entry box
    txtcappuser = ttk.Entry(pewel, bootstyle="success", width=10, text=gusr)
    # set that as a variable
    usr = txtcappuser.get()
    # Create path variable
    spath = "/home/" + usr + "/.config/autostart/"
    if os.path.isfile(spath + "Welcome_auto.desktop"):
        cbas = ttk.Checkbutton(
            pewel,
            text="Disable Auto start",
            bootstyle="light-round-toggle",
            command=kon_del)
        #cbas.grid(row=2, column=1)
        cbas.place(x=12, y=360)
    else:
        cbas = ttk.Checkbutton(
            pewel,
            text="Disable Auto start",
            bootstyle="light-round-toggle")


def kon_del():
    """ This feature will let the user disable the welcome screen"""
    # This will get the logged in user
    gusr = tk.StringVar()
    gusr.set(os.getlogin())
    # set it in to as entry box
    txtcappuser = ttk.Entry(pewel, bootstyle="danger", width=10, text=gusr)
    # set that as a variable
    usr = txtcappuser.get()
    # Create path variable
    spath = "/home/" + usr + "/.config/autostart/"
    if os.path.isfile(spath + "Welcome_auto.desktop"):
        os.remove(spath + "Welcome_auto.desktop")
        print("sym deleted")
    else:
        print("no welcome sym to delete")


# Set the window icon
pewel.tk.call('wm', 'iconphoto', pewel._w, tk.PhotoImage(
    file='/usr/share/pixmaps/peppermint-old.png'))

# logo for the splash
lpath2 = welconf.peppertxt
lg2 = tk.Label(pewel, image=lpath2, width=600, height=80, borderwidth=0)
lg2.grid(row=0, column=1, columnspan=2, ipadx=5, ipady=5, padx=5, pady=5)

# Frame that managed the Welcome To Peppermint Section
fsw = ttk.Frame(pewel, width=200)
fsw.grid(row=1, column=0, columnspan=4)

# Frame title
# Frame that manages the Community buttons
fs = ttk.Frame(fsw)
fs.grid(row=0, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='e')

wms = ttk.Labelframe(fs, bootstyle="warning", text="Welcome to Peppermint OS")
wms.grid(row=0, column=0, ipadx=0, ipady=0, padx=10, pady=10, sticky='e')


# Frame Buttons
btnsaw = ttk.Button(
    wms,
    text="Suggested",
    cursor="hand2",
    style="danger-outline",
    command=welfunc.suggested_packages)
btnsaw.grid(row=0, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

btnph = ttk.Button(
    wms,
    text="Peppermint Hub",
    cursor="hand2",
    bootstyle="danger-outline",
    command=welfunc.hub)
btnph.grid(row=1, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

btnpdocs = ttk.Button(
    wms,
    text="Pep Docs",
    cursor="hand2",
    bootstyle="danger-outline",
    command=welfunc.pep_docs)
btnpdocs.grid(row=2, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

btnbdl = ttk.Button(
    wms,
    text="Build Log",
    cursor="hand2",
    bootstyle="danger-outline",
    command=welfunc.build_date)
btnbdl.grid(row=3, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='ew')

# Frame labels
lblsaw = ttk.Label(wms, text=" - Select Packages and Web Browsers ",
                   wraplength=300)
lblsaw.grid(row=0, column=1, sticky='ew')

lblph = ttk.Label(wms, text=" - System changes, and customizations ",
                  wraplength=300)
lblph.grid(row=1, column=1, sticky='ew')

lblpdocs = ttk.Label(wms, text=" - Peppermint Online Documentation",
                     wraplength=300)
lblpdocs.grid(row=2, column=1, sticky='ew')

lblbdl = ttk.Label(wms, text=" - Review the build log",
                   wraplength=300)
lblbdl.grid(row=3, column=1, sticky='ew')

# Frame Title
soc = ttk.Labelframe(fs, bootstyle="warning", text="The Peppermint Community")
soc.grid(row=0, column=1, ipadx=5, ipady=5, padx=5, pady=5, sticky='e')
#soc.grid(row=5, column=0)

# Frame message
lblqt = ttk.Label(soc, text=welconf.MESSAGE_TEXT,
                  wraplength=250)
lblqt.grid(row=3, column=0, columnspan=3,sticky='ew')

# Social Icons
icotms = welconf.tms
icotfm = welconf.fm
icotmat = welconf.mat
icotcb = welconf.cb

# Social buttons
btncf = ttk.Button(
    soc,
    text="Forums",
    cursor="hand2",
    bootstyle="dark-outline",
    image=icotfm,
    command=welfunc.source_forge)
btncf.grid(
    row=0,
    column=0,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw'
    )
btnmt = ttk.Button(
    soc,
    text="-matrix-",
    cursor="hand2",
    bootstyle="dark-outline",
    image=icotmat,
    command=welfunc.matrix)
btnmt.grid(
    row=0,
    column=1,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw'
    )
btnmas = ttk.Button(
    soc,
    text="Mastodon",
    cursor="hand2",
    bootstyle="dark-outline",
    image=icotms,
    command=welfunc.mastodon)
btnmas.grid(
    row=1,
    column=1,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw'
    )
btncb = ttk.Button(
    soc,
    text="CodeBerg",
    cursor="hand2",
    bootstyle="dark-outline",
    image=icotcb,
    command=welfunc.code_berg)
btncb.grid(
    row=1,
    column=0,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw'
    )


# set the correct build branding
wbase()
kon()

# call Center screen
center_screen()
# run the application
pewel.mainloop()
