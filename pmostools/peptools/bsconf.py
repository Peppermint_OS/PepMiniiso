"""
* Author: "PeppermintOS Team(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This module will set the style to be used for bootstrap
* just change the name to the prefered theme to be used for the
* system
"""
import ttkbootstrap as ttk
bbstyle = ttk.Window(themename="darkly")
