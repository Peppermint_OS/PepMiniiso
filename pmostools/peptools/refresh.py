"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Used to refresh the data in the Kumo Gui
"""

import os


def refresh_win():
    """ Open Kumo"""
    # Set the user path
    gusr = os.getlogin()
    spath = "/home/" + gusr + "/.local/share/pmostools/peptools"
    os.system('python3 ' + spath + '/kumo.py')

refresh_win()



