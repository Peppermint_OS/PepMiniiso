# PeppermintOS Mini ISO Net Install

This project is a custom Mini ISO net install of PeppermintOS, licensed under the GNU General Public License v3.0. The Mini ISO allows users to easily and flexibly install PeppermintOS on their computer.

**The PeppermintOS Mini ISOs are now available for download and testing at this link: [PeppermintOS Mini ISOs](http://68.233.46.81/nightly/PepMini/)**

At the moment, an official ISO is not yet available for download. However, you can build the ISO locally if you wish to try it out. Once you have built the ISO, you can follow the instructions below to install PeppermintOS:

1. Burn the ISO image to a CD or create a bootable USB drive.
2. Boot your computer from the CD or USB drive.
3. Follow the on-screen instructions to install PeppermintOS using the Debian Installer.

Ensure that your computer meets the minimum system requirements for PeppermintOS. Also, be aware that the installation process will erase all content on your computer's hard drive.

## Features

This custom Mini ISO includes the following features:

- Installation of PeppermintOS using the Debian Installer.
- Option to install multiple desktops in addition to the Peppermint default desktop.
- Compatibility with i386, amd64, and arm64 architectures.

## Contribution

This project is open source, and contributions are welcome. If you would like to contribute, please follow the steps below:

1. Fork this repository.
2. Create a branch for your changes (git checkout -b my-branch).
3. Make your changes and ensure they work.
4. Submit a pull request to this repository.

All contributions are subject to the GPL 3 license.

## License

This project is licensed under the GNU General Public License v3.0. See the LICENSE.md file for more information.

## Author

This project was created by the PeppermintOS team. Please contact them if you have any questions or comments about the project.

## Acknowledgments

We thank the Debian team for providing the Debian Installer and the Tasksel project for allowing customization of the PeppermintOS installer.

