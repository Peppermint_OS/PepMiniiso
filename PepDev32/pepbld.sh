#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later
#
# SPDX-FileCopyrightText:  2023 PeppemrintOS Team  (peppermintosteam@proton.me)

PATH="/sbin:/usr/sbin:/usr/local/sbin:$PATH"

# Set the working folder variable
uchinanchu="$(pwd)"

# This cleanup might be better served in the BldHelper*.sh script.
# Create the build folder, move into it removing stale mountpoints and files there.
[ -e fusato ] && [ ! -d fusato ] && rm -f fusato || [ ! -e fusato ] && mkdir fusato
cd fusato
umount $(mount | grep "${PWD}/chroot" | tac | cut -f3 -d" ") 2>/dev/null
for i in ./* ./.build ; do [ $i = ./cache ] && continue || rm -rf $i ; done


# Defines Live Build settings
lb config  noauto \
	--binary-images iso-hybrid \
	--architectures i386 \
	--distribution daedalus \
	--initsystem sysvinit \
	--archive-areas "main contrib non-free non-free-firmware" \
	--mirror-bootstrap http://deb.devuan.org/merged \
	--parent-mirror-bootstrap http://deb.devuan.org/merged \
	--parent-mirror-chroot http://deb.devuan.org/merged \
	--parent-mirror-chroot-security http://deb.devuan.org/merged \
	--parent-mirror-binary http://deb.devuan.org/merged \
	--parent-mirror-binary-security http://deb.devuan.org/merged \
	--mirror-chroot http://deb.devuan.org/merged \
	--mirror-chroot-security http://deb.devuan.org/merged \
	--security false \
	--updates false \
	--backports false \
	--uefi-secure-boot enable \
	--firmware-chroot false \
	--debootstrap-options --include=zstd,locales,dialog,krb5-locales \
	--debian-installer cdrom\
	--debian-installer-distribution daedalus \
	--debian-installer-gui true \
	--parent-mirror-debian-installer http://deb.devuan.org/devuan \
	--iso-preparer "PeppermintOS-https://peppermintos.com/" \
	--iso-publisher "Peppermint OS Team" \
	--iso-volume "PeppermintOS" \
	--image-name "PepOS-mini-iso" \
	--checksums sha512 \
	--zsync false \
	--win32-loader false \
	--debian-installer-preseedfile preseed.cfg \

     "${@}"

# Setup the installer structure
mkdir -p $uchinanchu/fusato/config/includes.installer
mkdir -p $uchinanchu/fusato/config/includes.installer/usr/lib/finish-install.d
mkdir -p $uchinanchu/fusato/config/includes.installer/etc
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/repos
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/sources-final
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/keyrings
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/grub
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/apps
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/database
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/pixmaps
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/tools
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/protools
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/polkit
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/conf
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/py
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/info
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/lightdm
mkdir -p $uchinanchu/fusato/config/includes.installer/preseed/autostart
mkdir -p $uchinanchu/fusato/config/includes.installer/usr/share
mkdir -p $uchinanchu/fusato/config/hooks/normal

cp $uchinanchu/pepinstaller/preseed/preseed.cfg $uchinanchu/fusato/config/includes.installer
cp $uchinanchu/pephooks/normal/* $uchinanchu/fusato/config/hooks/normal
cp $uchinanchu/pepscriptsdev/* $uchinanchu/fusato/config/includes.installer/usr/lib/finish-install.d
cp $uchinanchu/pepautostart/* $uchinanchu/fusato/config/includes.installer/preseed/autostart
cp $uchinanchu/pepsources/multimedia.list $uchinanchu/fusato/config/includes.installer/preseed/repos
cp $uchinanchu/pepsources/peppermint.list $uchinanchu/fusato/config/includes.installer/preseed/repos
cp $uchinanchu/pepsources/sources.list $uchinanchu/fusato/config/includes.installer/preseed/sources-final
cp $uchinanchu/pepsources/sources.list $uchinanchu/fusato/config/includes.installer/preseed/repos
cp $uchinanchu/pepkeyrings/* $uchinanchu/fusato/config/includes.installer/preseed/keyrings
cp $uchinanchu/polkit/* $uchinanchu/fusato/config/includes.installer/preseed/polkit
cp $uchinanchu/pepapplication/* $uchinanchu/fusato/config/includes.installer/preseed/apps
cp $uchinanchu/pepdatabase/* $uchinanchu/fusato/config/includes.installer/preseed/database
cp $uchinanchu/PepProPixMaps/* $uchinanchu/fusato/config/includes.installer/preseed/pixmaps
cp $uchinanchu/pepconf/* $uchinanchu/fusato/config/includes.installer/preseed/conf
cp $uchinanchu/PepProTools/* $uchinanchu/fusato/config/includes.installer/preseed/protools
cp $uchinanchu/peplightdm/* $uchinanchu/fusato/config/includes.installer/preseed/lightdm
cp $uchinanchu/pepgrub/grub $uchinanchu/fusato/config/includes.installer/preseed/grub
cp $uchinanchu/pepinfo/* $uchinanchu/fusato/config/includes.installer/preseed/info

# Copy recursive files and sub-directories
cp -r $uchinanchu/peploadersplash/devuan/boot $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/peploadersplash/devuan/isolinux $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/pepinstaller/graphics $uchinanchu/fusato/config/includes.installer/usr/share
cp -r $uchinanchu/pepinstaller/themes $uchinanchu/fusato/config/includes.installer/usr/share
cp -r $uchinanchu/pepgrub/devuan/* $uchinanchu/fusato/config/includes.installer/preseed/grub
cp -r $uchinanchu/pylibraries/* $uchinanchu/fusato/config/includes.installer/preseed/py
cp -r $uchinanchu/pmostools/* $uchinanchu/fusato/config/includes.installer/preseed/tools

lb build
