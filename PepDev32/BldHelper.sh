 #!/bin/bash

################################################################################
# Title:        Bldhelper.sh
# Description:  Script to build PeppermintOS ISO image
# Author:       PeppermintOS Team <peppermintosteam@proton.me>
# Date:         May 10, 2023
# License:      GPL-3.0-or-later
################################################################################

# Set environment variables
PREFIX=PepMini
SUFFIX=dev-i386
BUILD=PepDev32
TODAY=$(date -u +"%Y-%m-%d")
FileName="${PREFIX}-${SUFFIX}"
LOCATION=/var/www/html/nightly/PepMini/${BUILD}
LogDir=/var/log/Live-Build
WorkingDir=/home/pepadmin/PepMini/PepDev32

# Execute the ISO building script
cd ${WorkingDir}
./pepbld.sh

# Move and rename the ISO file
cd fusato
mv *.iso ${FileName}-${TODAY}.iso

# Create the checksum file for the ISO
sha512sum ${FileName}-${TODAY}.iso > ${FileName}-${TODAY}-sha512.checksum

# Remove old ISO and checksum files from the desired location
rm -f ${LOCATION}/${FileName}*.iso
rm -f ${LOCATION}/${FileName}*-sha512.checksum

# Move the ISO and checksum files to the desired location
mkdir -p ${LOCATION}
mv ${FileName}-${TODAY}.iso ${LOCATION}
mv ${FileName}-${TODAY}-sha512.checksum ${LOCATION}

# Move the log file to the log directory (if it exists)
if [ -f /tmp/${PREFIX}${SUFFIX}.out ]; then
    mv /tmp/${PREFIX}${SUFFIX}.out ${LogDir}/${PREFIX}-${SUFFIX}-${BUILD}.log
fi

# Clean the build folder 
lb clean

# Remove the "fusato" directory and its contents
cd ..
rm -rf fusato
