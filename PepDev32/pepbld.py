#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later
#
# SPDX-FileCopyrightText: 2023 PeppermintOS Team  (peppermintosteam@proton.me)

import os
import subprocess
import sys
import shutil

# Set the working folder variable
uchinanchu = os.getcwd()

# Create the build folder, move into it removing stale mountpoints and files there.
if os.path.exists("fusato") and not os.path.isdir("fusato"):
    os.remove("fusato")
elif not os.path.exists("fusato"):
    os.makedirs("fusato")

os.chdir("fusato")
subprocess.run("umount $(mount | grep '${PWD}/chroot' | tac | cut -f3 -d' ') 2>/dev/null", shell=True)
for i in os.listdir() + ['.build']:
    if i == 'cache':
        continue
    else:
        os.system("rm -rf " + i)

# Defines Live Build settings
lb_command = [
    "lb", "config", "noauto",
    "--binary-images", "iso-hybrid",
    "--architectures", "i386",
    "--distribution", "daedalus",
    "--initsystem", "sysvinit",
    "--archive-areas", "main contrib non-free non-free-firmware",
    "--mirror-bootstrap", "http://deb.devuan.org/merged",
    "--parent-mirror-bootstrap", "http://deb.devuan.org/merged",
    "--parent-mirror-chroot", "http://deb.devuan.org/merged",
    "--parent-mirror-chroot-security", "http://deb.devuan.org/merged",
    "--parent-mirror-binary", "http://deb.devuan.org/merged",
    "--parent-mirror-binary-security", "http://deb.devuan.org/merged",
    "--mirror-chroot", "http://deb.devuan.org/merged",
    "--mirror-chroot-security", "http://deb.devuan.org/merged",
    "--security", "false",
    "--updates", "false",
    "--backports", "false",
    "--uefi-secure-boot", "enable",
    "--firmware-chroot", "false",
    "--debootstrap-options", "--include=zstd,locales,dialog,krb5-locales",
    "--debian-installer", "cdrom",
    "--debian-installer-distribution", "daedalus",
    "--debian-installer-gui", "true",
    "--parent-mirror-debian-installer", "http://deb.devuan.org/devuan",
    "--iso-preparer", "PeppermintOS-https://peppermintos.com/",
    "--iso-publisher", "Peppermint OS Team",
    "--iso-volume", "PeppermintOS",
    "--image-name", "PepOS-mini-iso",
    "--checksums", "sha512",
    "--zsync", "false",
    "--win32-loader", "false",
    "--debian-installer-preseedfile", "preseed.cfg"
]

subprocess.run(lb_command + sys.argv[1:])
# Setup the installer structure
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "usr", "lib", "finish-install.d"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "repos"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "sources-final"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "keyrings"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "grub"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "apps"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "database"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "pixmaps"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "tools"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "protools"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "polkit"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "conf"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "py"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "info"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "lightdm"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "usr", "share"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "autostart"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "hooks", "normal"), exist_ok=True)


shutil.copy(os.path.join(uchinanchu, "pepautostart", "Welcome_auto.desktop"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "autostart"))
shutil.copy(os.path.join(uchinanchu, "pepinstaller", "preseed", "preseed.cfg"), os.path.join(uchinanchu, "fusato", "config", "includes.installer"))
shutil.copy(os.path.join(uchinanchu, "pepsources", "multimedia.list"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "repos"))
shutil.copy(os.path.join(uchinanchu, "pepsources", "peppermint.list"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "repos"))
shutil.copy(os.path.join(uchinanchu, "pepsources", "sources.list"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "sources-final"))
shutil.copy(os.path.join(uchinanchu, "pepsources", "sources.list"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "repos"))
shutil.copy(os.path.join(uchinanchu, "pepgrub", "grub"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "grub"))

# Copy recursive files and sub-directories
shutil.copytree(os.path.join(uchinanchu, "pepscriptsdev"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "usr", "lib", "finish-install.d"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pephooks", "normal"), os.path.join(uchinanchu, "fusato", "config", "hooks", "normal"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pepkeyrings"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "keyrings"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "polkit"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "polkit"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pepapplication"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "apps"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pepdatabase"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "database"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "PepProPixMaps"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "pixmaps"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pepconf"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "conf"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "PepProTools"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "protools"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "peplightdm"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "lightdm"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "peploadersplash", "devuan"), os.path.join(uchinanchu, "fusato", "config", "includes.binary"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pepinstaller"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "usr", "share"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pepgrub", "devuan"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "grub"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pylibraries"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "py"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pmostools"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "tools"), dirs_exist_ok=True)
shutil.copytree(os.path.join(uchinanchu, "pepinfo"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "info"), dirs_exist_ok=True)

subprocess.run("lb build", shell=True)
